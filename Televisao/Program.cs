﻿
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Televisao
{
    using System;

    class Program
    {

        static void Main(string[] args)
        {
            Tv minhaTv = new Tv();

            Console.WriteLine(minhaTv.EnviaMensagem());
            Console.WriteLine(minhaTv.GetCanal());
            Console.WriteLine(minhaTv.GetVolume());

            bool sair = false;

            int op;

            do
            {
                MenuLigarTv();

                do
                {
                     int.TryParse(Console.ReadLine(), out op);

                } while (op != 0 && op != 1);

                switch (op)
                {
                    case 1:
                        minhaTv.LigaTv();
                        Console.WriteLine(minhaTv.EnviaMensagem());
                        Console.WriteLine("Canal: {0}  Volume:{1}", minhaTv.GetCanal(), minhaTv.GetVolume());
                        int op2;
          
                        do
                        {
                            MenuTv();
                            int.TryParse(Console.ReadLine(), out op2);

                            switch (op2)
                            {
                                case 1:
                                    int canal;
                                    Console.Clear();
                                    Console.Write("Insira o canal pretendido:");
                                    int.TryParse(Console.ReadLine(), out canal);

                                    if (canal > 0)
                                    {
                                        minhaTv.MudaCanal(canal);
                                    }

                                    op2 = 10;
                                    break;

                                case 2:

                                    int volume;
                                    Console.Clear();
                                    Console.Write("Insira volume pretendido para aumentar ou diminuir:");
                                    int.TryParse(Console.ReadLine(), out volume);

                                    if (volume > minhaTv.GetVolume())
                                    {
                                        minhaTv.AumentaVolume(volume);
                                    }
                                    else if (volume < minhaTv.GetVolume())
                                    {
                                        minhaTv.DiminuiVolume(volume);
                                    }

                                    op2 = 11;
                                    break;

                                case 3:
                                    minhaTv.DesligaTv();
                                    Console.WriteLine(minhaTv.EnviaMensagem());
                                    break;
                            }

                        } while (op2 < 1 || op2 > 3);
                        break;

                    case 0:
                        Console.WriteLine("Vai sair do programa");
                        sair = true;
                        break;
                }
            } while (!sair);
        }

        static void MenuLigarTv()
        {
            
            Console.WriteLine("--------------------Menu Ligar Tv--------------------");
            Console.WriteLine("1-Ligar TV");
            Console.WriteLine("0-Sair do programa");
            Console.Write("Insira uma op:");
        }


        static void MenuTv()
        {
            Console.WriteLine("--------------------Menu da Televisão--------------------");
            Console.WriteLine("1-Alterar Canal");
            Console.WriteLine("2-Alterar Volume");
            Console.WriteLine("3-Desligar TV");
            Console.Write("Insira uma op:");
        }
    }
}
